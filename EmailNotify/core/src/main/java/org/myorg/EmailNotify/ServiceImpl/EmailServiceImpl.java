package org.myorg.EmailNotify.ServiceImpl;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service({ EmailServiceImpl.class })
@Component(immediate = true, metatype = true, label = "Custom emailService")

public class EmailServiceImpl {

	private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);

	@SuppressWarnings("rawtypes")
	public String sendEmail(Map<String, Object> map, SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws IOException {

		try {

			// String to = "pavani@threeuk.com";
			String from = "pavanidepatla@threeuk.com";

			Properties properties = new Properties();
			properties.put("mail.user", from);
			properties.put("mail.password", "maY@2017");
			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				LOG.info(pair.getKey() + " = " + pair.getValue());

				properties.put(pair.getKey(), String.valueOf(pair.getValue()));

			}
			LOG.info("propert value..." + properties.size());

			Session session = Session.getInstance(properties);
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(request.getParameter("toAddress")));
			msg.setSubject(request.getParameter("subject"));
			msg.setText(request.getParameter("message"));
			Transport.send(msg);
			response.getWriter().println("Sent message successfully....");
		}

		catch (Exception ex) {
			response.getWriter().println("Exception" + ex);
			ex.printStackTrace();

		}
		return "sucessfully sent";
	}

}
