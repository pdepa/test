package org.myorg.EmailNotify.ServiceImpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.PropertyType;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
@SlingServlet(paths = "/bin/CustomEmail", methods = "GET")
public class EmailServiceServlet extends SlingAllMethodsServlet {
	private static final Logger LOG = LoggerFactory.getLogger(EmailServiceImpl.class);
	@Reference
	EmailServiceImpl emailServiceImpl;
	
	Map<String, Object> map = new HashMap<String, Object>();

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		Session session = request.getResourceResolver().adaptTo(Session.class);

		Node node;
		try {
			node = session
					.getNode("/apps/emailnotify/config/org.myorg.EmailNotify.ServiceImpl.ConfigurationServiceImpl");

			/*PropertyIterator prty = node.getProperties();

			while (prty.hasNext()) {

				Property property = prty.nextProperty();

				map.put(property.getName(), property.getValue());
			}
			
*/
			 String prop1 = node.getProperty("mail.smtp.host").getValue().getString();
			 String prop2 = node.getProperty("mail.smtp.port").getValue().getString();
			 map.put("mail.smtp.host", prop1);
			 map.put("mail.smtp.port", prop2);
			emailServiceImpl.sendEmail(map,request,response);

		} catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}